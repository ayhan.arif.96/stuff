NgModules collect related code into functional sets; an Angular application is defined by a set of NgModules. 
An application always has at least a root module that enables bootstrapping, and typically has many more feature modules.

Event binding lets your application respond to user input in the target environment by updating your application data.
Property binding lets you interpolate values that are computed from your application data into the HTML.

Before a view is displayed, Angular evaluates the directives and resolves the binding syntax in the template to modify the HTML elements and the DOM
Angular supports two-way data binding, meaning that changes in the DOM, such as user choices, are also reflected in your program data.

@ViewChild decorator is a template querying mechanism that is local to the component.

The @ViewChild and @ViewChildren decorators in Angular provide access to child elements in the view DOM by setting up view queries. 
A view query is a requested reference to a child element within a component view which contains metadata of the element. 
The scope of these decorators is limited to the component view and its embedded child views. 
These decorators are especially helpful in instances where being able to access and modify elements within the view in conventional ways is not possible.

If we want to write component initialization code that uses the references injected by @ViewChild, we need to do it inside the AfterViewInit lifecycle hook.
The @ViewChild decorator cannot see across component boundaries!
Anything inside the templates of its child components
and neither anything in the template of parent components as well

@ViewChildren decorator works similarly to the @ViewChild decorator but instead of configuring a single view query, it gets a query list. 
From the component view DOM, it retrieves a QueryList of child elements. This list is updated when any changes are made to the child elements.
static If it is true, the view query is resolved before the complete competent view and data-bound properties are fully initialized.

As the name suggests, @ContentChild and @ContentChildren queries will return directives existing inside the <ng-content></ng-content> element of your view, 
whereas @ViewChild and @ViewChildren only look at elements that are on your view template directly.

ng-content is used to project content at a specific place within a component.
ng-template is a block describing a template, a template is not rendered unless explicitly referenced or used.
ng-container is another Angular element used with structural directives (ngFor, ngIf), it does not produce HTML either.

parent.html
<ng-content></ng-content>
<ng-content select="[question]"></ng-content>

grant-parent.html
<app-zippy-multislot>
  <p question>
    Is content projection cool?
  </p>
  <p>Let's learn about content projection!</p>
</app-zippy-multislot>

The Renderer2 class is an abstraction provided by Angular in the form of a service that allows to manipulate elements of your app without having to touch the DOM directly. 
This is the recommended approach because it then makes it easier to develop apps that can be rendered in environments that don’t have DOM access, like on the server, in a web worker or on native mobile.
Notice how we also use ElementRef to get access to the underlining native element that our directive is attached-to.

@Input() and @Output() give a child component a way to communicate with its parent component. @Input() lets a parent component update data in the child component. 
Conversely, @Output() lets the child send data to a parent component.
To watch for changes on an @Input() property, use OnChanges, one of Angular's lifecycle hooks.

The @Output() decorator in a child component or directive lets data flow from the child to the parent.
The child component uses the @Output() property to raise an event to notify the parent of the change. 
To raise an event, an @Output() must have the type of EventEmitter, which is a class in @angular/core that you use to emit custom events.

Use pipes to transform strings, currency amounts, dates, and other data for display. Pipes are simple functions to use in template expressions to accept an input value and return a transformed value.
To apply a pipe, use the pipe operator (|) within a template expression

Directives are classes that add additional behavior to elements in your Angular applications. With Angular's built-in directives, you can manage forms, lists, styles, and what users see.
directive that can listen to and modify the behavior of other HTML elements, attributes, properties, and components.

Structural directives
NgIf
NgFor
NgSwitch

Built-in attribute directives
ng g directive directive-name
NgClass—adds and removes a set of CSS classes.
NgStyle—adds and removes a set of HTML styles.
NgModel—adds two-way data binding to an HTML form element. Import FormsModule and add it to the NgModule's imports list.[(ngModel)]="currentItem.name"

attribute directive 
The @Directive() decorator's configuration property specifies the directive's CSS attribute selector, [appHighlight]
mport ElementRef from @angular/core. ElementRef grants direct access to the host DOM element through its nativeElement property.
handling user events
@HostListener('mouseenter') onMouseEnter() {
  this.highlight('yellow');
}
Import HostListener from '@angular/core'.
With the @HostListener() decorator, you can subscribe to events of the DOM element that hosts an attribute directive, the <p> in this case.
Passing values into an attribute directive
In highlight.directive.ts, import Input from @angular/core.
Add an appHighlight @Input() property.
The @Input() decorator adds metadata to the class that makes the directive's appHighlight property available for binding.
To simultaneously apply the directive and the color, use property binding with the appHighlight directive selector, setting it equal to color.
The [appHighlight] attribute binding performs two tasks:
applies the highlighting directive to the <p> element
sets the directive's highlight color with a property binding
same for multiple parameters
[selectable] = 'opt' 
[first]='YourParameterHere'

============= ROUTING =================
in order to get queryParams
Inject an instance of ActivatedRoute by adding it to your application's constructor: -> route
Update the ngOnInit() method to access the ActivatedRoute and track the id parameter:
'queryParams' can be replaced with 'params'
 1. Snapshot (route.snapshot.paramMap.get('id')). Read it during init. //sync
 2. Observable (route.paramMap.subscribe). Subscribe to it during init. -> //async
	Use the Observable if it's possible for the route to change 
	while the user is still on the same component, and hence the Component's initialization would not be called again, but the observable
   	would call your subscribed logic when the URL changed.
this.route.paramMap.subscribe(params: ParamMap => {
        this.id = +params.get('id');
        console.log(this.id);     
  });

    // Initialize Params Object
    let params = new HttpParams();

    // Begin assigning parameters
    params = params.append('firstParameter', valueOne);
    params = params.append('secondParameter', valueTwo);

    // Make the API call using the new parameters.
    return this._HttpClient.get(`${API_URL}/api/v1/data/logs`, { params: params })

this.router.navigate([ url ], { queryParams: { ... } })

route guards
ng g guard guard-name
export class YourGuard implements CanActivate {
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
      if (!this.auth.isAuthenticated()) {
      this.router.navigate(['login']);
      return false;
    }
    return true;
  }
}

link parameters array
<a [routerLink]="['/crisis-center']">Crisis Center</a>
<a [routerLink]="['/crisis-center/1', { foo: 'foo' }]">Dragon Crisis</a>
<a [routerLink]="['/crisis-center/2']">Shark Crisis</a>

Use the HttpClient.get() method to fetch data from a server. returns an Observable of the response body as a JSON object.
The asynchronous method sends an HTTP request, and returns an Observable that emits the requested data 
when the response is received. The return type varies based on the observe and responseType values 
that you pass to the call.

jsonp()
The JSONP pattern works around limitations of certain API endpoints that don't support newer, and preferable CORS protocol.
JSONP treats the endpoint API as a JavaScript file and tricks the browser to process the requests 
even if the API endpoint is not located on the same domain (origin)

The AsyncPipe subscribes (and unsubscribes) for you automatically.
Unwraps a value from an asynchronous primitive.
The async pipe subscribes to an Observable or Promise and returns the latest value it has emitted. 
When a new value is emitted, the async pipe marks the component to be checked for changes. 
When the component gets destroyed, the async pipe unsubscribes automatically to avoid potential memory leaks.

Use the HttpParams class with the params request option to add URL query strings in your HttpRequest.

HttpInterceptor

Intercepts and handles an HttpRequest or HttpResponse.
req	HttpRequest	
	The outgoing request object to handle.

next	HttpHandler	
	The next interceptor in the chain, or the backend if no interceptors remain in the chain.

Write an interceptor
To implement an interceptor, declare a class that implements the intercept() method of the HttpInterceptor interface.
The intercept method transforms a request into an Observable that eventually returns the HTTP response. 
In this sense, each interceptor is fully capable of handling the request entirely by itself.
Like intercept(), the handle() method transforms an HTTP request into an Observable of HttpEvents which ultimately include the server's response. 
The intercept() method could inspect that observable and alter it before returning it to the caller.

This app provides HttpClient in the app's root injector, as a side-effect of importing the HttpClientModule in AppModule. 
You should provide interceptors in AppModule as well.
After importing the HTTP_INTERCEPTORS injection token from @angular/common/http, write the NoopInterceptor provider like this:
{ provide: HTTP_INTERCEPTORS, useClass: NoopInterceptor, multi: true },

import { AuthService } from '../auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private auth: AuthService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    // Get the auth token from the service.
    const authToken = this.auth.getAuthorizationToken();

    // Clone the request and replace the original headers with
    // cloned headers, updated with the authorization.
    const authReq = req.clone({
      headers: req.headers.set('Authorization', authToken)
    });

    // send cloned request with header to the next handler.
    return next.handle(authReq);
  }
}

RXJS
Creation		from, fromEvent, of
Combination		combineLatest, concat, merge, startWith , withLatestFrom, zip
Filtering		debounceTime, distinctUntilChanged, filter, take, takeUntil
Transformation		bufferTime, concatMap, map, mergeMap, scan, switchMap
Utility			tap
Multicasting		share

============= RXJS ================
RxJS is a library for composing asynchronous and event-based programs by using observable sequences
Think of RxJS as Lodash for events.
ReactiveX combines the Observer pattern with the Iterator pattern and functional programming with collections to fill the need for an ideal way of managing sequences of events.

The essential concepts in RxJS which solve async event management are:
	Observable: represents the idea of an invokable collection of future values or events.
	Observer: is a collection of callbacks that knows how to listen to values delivered by the Observable.
	Subscription: represents the execution of an Observable, is primarily useful for cancelling the execution.
	Operators: are pure functions that enable a functional programming style of dealing with collections with operations like map, filter, concat, reduce, etc.
	Subject: is equivalent to an EventEmitter, and the only way of multicasting a value or event to multiple Observers.
	Schedulers: are centralized dispatchers to control concurrency, allowing us to coordinate when computation happens on e.g. setTimeout or requestAnimationFrame or others.
Operators are functions. There are two kinds of operators:

Pipeable Operators are the kind that can be piped to Observables using the syntax observableInstance.pipe(operator())
A Pipeable Operator is a function that takes an Observable as its input and returns another Observable. It is a pure operation: the previous Observable stays unmodified.
Creation Operators are the other kind of operator, which can be called as standalone functions to create a new Observable. For example: of(1, 2, 3) 

takeUntil subscribes and begins mirroring the source Observable. It also monitors a second Observable, notifier that you provide. If the notifier emits a value, 
the output Observable stops mirroring the source Observable and completes. If the notifier doesn't emit any value and completes then takeUntil will pass all values.
	example
	1.  destroy$: Subject<boolean> = new Subject<boolean>();
	2. .pipe(takeUntil(this.destroy$))
	3.  ngOnDestroy() {
   		 this.destroy$.next(true);
	         this.destroy$.unsubscribe();
  	   }
take(1) you only want the first value

tap Jumping outside of stream is possible with 'tap' operator, where you can: 1: call functions that will cause some side effect (eg: display dialog) 2: dispatch actions for store

observable
A producer of multiple values, which it pushes to subscribers. Used for asynchronous event handling throughout Angular. 
You execute an observable by subscribing to it with its subscribe() method, passing callbacks for notifications of new values, errors, or completion.

You need to call subscribe() to produce a result through the recipe.

observables because they are lazy. This means that nothing happens until a subscription is made. While promises handle a single event, observable is a stream that allows passing of more than one event. 
A callback is made for each event in an observable.

A Subscription essentially just has an unsubscribe() function to release resources or cancel Observable executions.

You can use pipes to link operators together. Pipes let you combine multiple functions into a single function. 
The pipe() function takes as its arguments the functions you want to combine, and returns a new function that, when executed, runs the composed functions in sequence.

An RxJS Subject is a special type of Observable that allows values to be multicasted to many Observers.
Each next subscribers receive...
Subject	...only upcoming values
BehaviorSubject	...one previous value and upcoming values
ReplaySubject	...all previous values and upcoming values
AsyncSubject	...the latest value when the stream will close

============= FORMS =============
Reactive forms are more scalable than template-driven forms. They provide direct access to the underlying form API, 
and use synchronous data flow between the view and the data model, which makes creating large-scale forms easier.

FormControl tracks the value and validation status of an individual form control.
	[formControl]="favoriteColorControl"
	add property binding to html el.
FormGroup tracks the same values and status for a collection of form controls.

FormArray tracks the same values and status for an array of form controls.

ControlValueAccessor creates a bridge between Angular FormControl instances and native DOM elements.

ngSubmit: EventEmitter - Event emitter for the "ngSubmit" event

*ngIf="formGroup.get('toDo').hasError('required')" // show error message

 // custom validator
 passwordCompareValidator(controlName: string, checkControlName: string): ValidatorFn {
        return (controls: AbstractControl): ValidationErrors | null => {
            const control = controls.get(controlName);
            const checkControl = controls.get(checkControlName);

            if (checkControl!.errors && !checkControl!.errors.invalidCompare) {
                return null;
            }

            if (control!.value !== checkControl!.value) {
                controls.get(checkControlName)!.setErrors({ 'invalidCompare': true });
                return ({ invalidCompare: true });
            } else {
                return null;
            }
        };
    }

================== HOOKS ================
== ngOnChanges(changes: SimpleChanges) ==
Angular calls its ngOnChanges() method whenever it detects changes to input properties of the component (or directive)
Respond when Angular (re)sets data-bound input properties. The method receives a SimpleChanges object of current and previous property values.
Called before ngOnInit() and whenever one or more data-bound input properties change.

== ngOnInit() == 
nitialize the directive/component after Angular first displays the data-bound properties and sets the directive/component's input properties.
Called once, after the first ngOnChanges().
	To perform complex initializations shortly after construction.
	To set up the component after Angular sets the input properties.
	http requests
	is a one-and-done hook. Initialization is its only concern.

== ngDoCheck() ==
Detect and act upon changes that Angular can't or won't detect on its own.
Called during every change detection run, immediately after ngOnChanges() and ngOnInit().

== ngAfterContentInit()	==
Respond after Angular projects external content into the component's view.
Called once after the first ngDoCheck().

== ngAfterContentChecked() ==
Respond after Angular checks the content projected into the component.
Called after the ngAfterContentInit() and every subsequent ngDoCheck().
	You can use ContentChild to get the first element or the directive matching the selector from the content DOM. 
	If the content DOM changes, and a new child matches the selector, the property will be updated.

== ngAfterViewInit() ==
Respond after Angular initializes the component's views and child views.
Called once after the first ngAfterContentChecked().
	viewChild is set after the view has been initialized

== ngAfterViewChecked() ==
Respond after Angular checks the component's views and child views.
Called after the ngAfterViewInit and every subsequent ngAfterContentChecked().
	viewChild is updated after the view has been checked	

== ngOnDestroy ==
Cleanup just before Angular destroys the directive/component. Unsubscribe Observables and detach event handlers to avoid memory leaks.
Called just before Angular destroys the directive/component.

ngOnChanges: When an input/output binding value changes.
ngOnInit: After the first ngOnChanges. is a one-and-done hook. Initialization is its only concern.
ngDoCheck: Developer's custom change detection.
ngAfterContentInit: After component content initialized.
ngAfterContentChecked: After every check of component content.
ngAfterViewInit: After a component's views are initialized.
ngAfterViewChecked: After every check of a component's views.
ngOnDestroy: Just before the directive is destroyed.




