function findDuplicatesInArray(arr) {
  let object = {}; // Създаваме обект, в който ще пазим броя на срещанията на съответните елементи в масива
  let result = []; // Създаваме масив, който ще съхраняваме повтарящите се елементи и ще върнем в края на функцията
  let sortedArr; // Променлива, чията стойност ще е сортираният масив
  let paragraph = document.getElementById("paragraph"); // Достъпваме <p> елемента

  // С помощта на for цикъл обхождаме arr
  for (let i = 0; i < arr.length; i++) {
    let item = arr[i];

    debugger
    if (!object[item]) {
      object[item] = 0; // Ако нямаме създаден запис за съответния елемент от масива, то създаваме такъв с брой 0
    }

    object[item] += 1; // Увеличаваме броя на записите с 1
  }

  // С помощта на for цикъл обхождаме всички записи в обекта
  for (let prop in object) {
    // Ако броят на съответния запис е по-голям или равен на 2 - добавяме го в result масива, т.е. елементът се повтаря
    if (object[prop] >= 2) {
      let propInteger = parseInt(prop);
      result.push(propInteger);
    }
  }

  sortedArr = result.sort(function (a, b) {
    return a - b;
  });

  paragraph.innerHTML = sortedArr;
}
