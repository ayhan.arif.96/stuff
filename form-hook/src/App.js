import "./App.scss";
import LoginForm from "./components/LoginForm/LoginForm";
import { SignupForm } from "./components/SignupForm/SignupForm";

function App() {
    return (
        <>
            <h1>Form Validation</h1>
            <SignupForm
                onSubmit={(e) => {
                    console.log("res", e);
                }}
            />
            <br />
            <br />
            <br />
            <LoginForm />
        </>
    );
}

export default App;
