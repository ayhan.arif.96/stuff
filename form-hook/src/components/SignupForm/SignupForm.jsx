import { isRequired, isSame, useForm } from './../../hooks/useForm';
import { useEffect } from 'react';
import axios from 'axios';

export const SignupForm = ({ onSubmit }) => {
    const initialState = { firstName: '', lastName: '', email: '', password: '', repeatPassword: '' };
    const validations = [
        ({ firstName }) => isRequired(firstName) || { firstName: 'First name is required' },
        ({ lastName }) => isRequired(lastName) || { lastName: 'Last name is required' },
        ({ email }) => isRequired(email) || { email: 'E-mail is required' },
        ({ password }) => isRequired(password) || { password: 'Password is required' },
        ({ password, repeatPassword }) => isSame(password, repeatPassword) || { repeatPassword: 'Passwords do not match' }
    ];
    const { values, errors, touched, isValid, changeHandler, submitHandler } = useForm(initialState, validations, onSubmit);

    useEffect(() => {
        const fetchData = async () => {

            // const axiosRes = await axios.get("https://jsonplaceholder.typicode.com/todos/3"); -> get value from data property
            // console.log("🚀 ~ file: SignupForm.jsx:20 ~ fetchData ~ axiosRes", axiosRes)

            // adding await before get reponse will trgiger the response
            const axiosRes1 =  axios.get("https://jsonplaceholder.typicode.com/todos/3");
            const axiosRes2 =  axios.get("https://jsonplaceholder.typicode.com/todos/4");
            const [ax1, ax2] = await Promise.all([axiosRes1, axiosRes2]);

            console.log("🚀 ~ file: SignupForm.jsx:23 ~ fetchData ~ ax1", ax1)
            console.log("🚀 ~ file: SignupForm.jsx:23 ~ fetchData ~ ax2", ax2)

            const data1 = fetch('https://jsonplaceholder.typicode.com/todos/1');
            // const json1 = await data1.json();
            // console.log("🚀 ~ file: SignupForm.jsx ~ line 19 ~ fetchData ~ json1", json1)

            const data2 = fetch('https://jsonplaceholder.typicode.com/todos/2');
            // const json2 = await data2.json();
            // console.log("🚀 ~ file: SignupForm.jsx ~ line 22 ~ fetchData ~ json2", json2)

            const [res1, res2] = await Promise.all([data1, data2]);
            const [r1, r2] = await Promise.all([res1.json(), res2.json()]);
            console.log("🚀 ~ file: SignupForm.jsx ~ line 26 ~ fetchData ~ r1", r1)
            console.log("🚀 ~ file: SignupForm.jsx ~ line 26 ~ fetchData ~ r2", r2)
        }
        fetchData();
    }, []);

    // for (let index = 0; index < 5; index++) {
    //     const createdDiv = document.createElement("div");
    //     createdDiv.classList.add("test");
    //     const newContent = document.createTextNode("Hi there and greetings!");
    //     createdDiv.appendChild(newContent);
    //     document.body.append(createdDiv);
    // }

    return (
        <form
            className="signup-form"
            onSubmit={submitHandler}>
            <h2>Sign up</h2>
            <div className="form-fields">
                <div className="first-name">
                    <label>First name</label>
                    <input
                        type="text"
                        placeholder="ex. Harry"
                        name="firstName"
                        required
                        value={values.firstName}
                        onChange={changeHandler} />
                    {touched.firstName && errors.firstName && <p className="error">{errors.firstName}</p>}
                </div>
                <div className="last-name">
                    <label>Last name</label>
                    <input
                        type="text"
                        placeholder="ex. Potter"
                        name="lastName"
                        required
                        value={values.lastName}
                        onChange={changeHandler} />
                    {touched.lastName && errors.lastName && <p className="error">{errors.lastName}</p>}
                </div>
                <div className="email">
                    <label>E-mail</label>
                    <input
                        type="email"
                        placeholder="ex. harry.potter@example.org"
                        name="email"
                        required
                        value={values.email}
                        onChange={changeHandler} />
                    {touched.email && errors.email && <p className="error">{errors.email}</p>}
                </div>
                <div className="password">
                    <label>Password</label>
                    <input
                        type="password"
                        placeholder="ex. Pa$$w0rd"
                        name="password"
                        required
                        value={values.password}
                        onChange={changeHandler} />
                    {touched.password && errors.password && <p className="error">{errors.password}</p>}
                </div>
                <div className="repeat-password">
                    <label>Repeat password</label>
                    <input
                        type="password"
                        placeholder="ex. Pa$$w0rd"
                        name="repeatPassword"
                        required
                        value={values.repeatPassword}
                        onChange={changeHandler} />
                    {touched.repeatPassword && errors.repeatPassword && <p className="error">{errors.repeatPassword}</p>}
                </div>
            </div>
            <div className="actions">
                <button
                    disabled={!isValid}>
                    Sign up
                </button>
            </div>
        </form>
    );
};