import useValidate, { isRequired } from './../../hooks/useValidate';

const validations = [
    ({ email }) => isRequired(email) || { email: 'Email is required' },
    ({ password }) => isRequired(password) || { password: 'Password is required' },
]

const LoginForm = (props) => {
    const initialState = { email: '', password: '' };

    const { values, handleChange, submitHandler, isValid, errors, touched } = useValidate(initialState, validations);

    return (
        <form
            className="signup-form"
            onSubmit={submitHandler}>
            <h2>Sign up</h2>
            <div className="form-fields">
                <div className="email">
                    <label>E-mail</label>
                    <input
                        type="email"
                        placeholder="ex. harry.potter@example.org"
                        name="email"
                        required
                        value={values.email}
                        onChange={handleChange} />
                    {touched.email && errors.email && <p className="error">{errors.email}</p>}
                </div>
                <div className="password">
                    <label>Password</label>
                    <input
                        type="password"
                        placeholder="ex. Pa$$w0rd"
                        name="password"
                        required
                        value={values.password}
                        onChange={handleChange} />
                    {touched.password && errors.password && <p className="error">{errors.password}</p>}
                </div>
            </div>
            <div className="actions">
                <button
                    disabled={!isValid}>
                    Sign up
                </button>
            </div>
        </form >
    );
}

export default LoginForm;