/**
 // useage
 * const {fetchData: fetchAllUsers} = useFetch({setIsPendingAllUsers, setErrorAllUsers, setData: setAllUsersData});
 *
 * OR
 * const {fetchData: fetchAllUsers} = useFetch();
 * 
 * OR 
 * in async function you can await, const response = await fetchAllUsers(url, method, body...)
 */

const useFetch = ({
  setIsPending,
  setError,
  setData,
  baseUrl,
  navigate,
  requestHeaders,
} = {}) => {
  const apiBaseUrl = baseUrl ?? "/api/v1/";

  const fetchData = async (endPoint, method = "GET", body = {}) => {
    let options = {};
    const gwaAccessToken = localStorage.getItem("gwaAccessToken");
    const gwaTokenType = localStorage.getItem("gwaTokenType");
    const gwaExpiresAt = Number(localStorage.getItem("gwaExpiresAt"));
    //If token is expired, do not send a request, but go to login page.
    const tokenHasExpired =
      gwaAccessToken && gwaExpiresAt && gwaExpiresAt < new Date().getTime();
    if (navigate && (!gwaAccessToken || tokenHasExpired)) {
      navigate("/login");
      return;
    }
    //If token is not expired, continue with using fetch.
    setIsPending && setIsPending(true);

    let headers = { "Content-Type": "application/json" };
    if (gwaTokenType && gwaAccessToken) {
      headers.Authorization = `${gwaTokenType} ${gwaAccessToken}`;
    }
    if (requestHeaders) {
      for (const header in requestHeaders) {
        headers[header] = requestHeaders[header];
      }
    }

    options = {
      method: method,
      headers,
    };
    if (body && ["POST", "PUT"].includes(method)) {
      options.body = JSON.stringify(body);
    }
    return fetch(apiBaseUrl + endPoint, options)
      .then(async (res) => {
        if (!res.ok) {
          // errors -> are custom BE messages that are promise
          // eslint-disable-next-line no-throw-literal
          throw { errorObj: res, errors: await res.json() };
        }
        return res.json();
      })
      .then((data) => {
        setIsPending && setIsPending(false);
        const result = data.results || data;
        setData && setData(result);
        setError && setError(null);
        return data;
      })
      .catch((err) => {
        // use errors to show some app level alerts
        const { errors, errorObj } = err;
        setIsPending && setIsPending(false);
        setError && setError(errorObj.message);
        return errorObj;
      });
  };

  return { fetchData };
};

export default useFetch;
