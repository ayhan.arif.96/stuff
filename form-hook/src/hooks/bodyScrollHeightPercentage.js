export default function bodyScrollHeightPercentage() {
  const scrollTop = window.scrollY;
  const docHeight = document.body.offsetHeight;
  const winHeight = window.innerHeight;
  const scrollPercent = scrollTop / (docHeight - winHeight);
  const scrollPercentRounded = Math.round(scrollPercent * 100);
  console.log(
    "🚀 ~ file: bodyScrollHeightPercentage.js:7 ~ bodyScrollHeightPercentage ~ scrollPercentRounded:",
    scrollPercentRounded
  );
}
