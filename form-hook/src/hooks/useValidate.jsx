import { useState } from "react";

function validate(validations, values) {
  const errors = validations
    .map((validation) => validation(values))
    .filter((validation) => typeof validation === "object");
  return {
    isValid: errors.length === 0,
    errors: errors.reduce((errors, error) => ({ ...errors, ...error }), {}),
  };
}

const useValidate = (
  initialState = {},
  validations = [],
  handleSubmit = () => {}
) => {
  const { isValid: initialIsValid, errors: initialErrors } = validate(
    validations,
    initialState
  );
  const [values, setValues] = useState(initialState);
  const [isValid, setValid] = useState(initialIsValid);
  const [errors, setErrors] = useState(initialErrors);
  const [touched, setTouched] = useState({});

  const handleChange = ({ target: { value, name } }) => {
    const newValues = { ...values, [name]: value };
    setValues(newValues);
    const { isValid, errors } = validate(validations, newValues);
    setValid(isValid);
    setErrors(errors);
    setTouched({ ...touched, [name]: true });
  };

  const submitHandler = (event) => {
    event.preventDefault();
    console.log("values", values);
  };

  return { values, handleChange, submitHandler, isValid, errors, touched };
};

export function isRequired(value) {
  return value != null && value.trim().length > 0;
}

export default useValidate;
